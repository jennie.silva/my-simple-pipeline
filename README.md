# My simple pipeline

# Desafio 06

Agora é hora de automatizarmos o deploy da nossa aplicação. Para isso, vamos utilizar uma pipeline.

## Atividade

Criar uma pipeline utilizando qualquer ferramenta de CI/CD e fazer o deploy da sua aplicação no kubernetes.

## Critérios de aceite / sucesso

* assim que um commit for realizado na sua branch master, o pipeline deve ser automaticamente iniciado
* build e publicação da imagem do contêiner
* deploy no Kubernetes da sua aplicação (utilizando os yamls do desafio 5)
* teste da sua aplicação
* Em caso de falha no teste, efetuar o rollback da aplicação
* Readme com a expliação de por que escolheu essa ferramenta de CI/CD e como configurá-la

## Referências

Pipeline: https://gomex.me/2020/05/28/qual-software-de-pipeline-ci/cd-voc%C3%AA-deve-usar/

Pipeline na MUC: https://muc.mandic.com.br/mod/scorm/player.php?a=89&currentorg=gitlab_ci_pipelines%2C_continuous_delivery_e_deployment_organization&scoid=224

------------------------------------------------------------------------------------------------------------------------------------------------------

## Desafio 06

Criar uma pipeline utilizando qualquer ferramenta de CI/CD e fazer o deploy da sua aplicação no kubernetes.


Neste projeto foi utilizado a ferramenta Gitlab CI/CD juntamente com o Gitlab Runner.

O Gitlab oferece um serviço de integração contínua. Para cada confirmação ou push para acionar o pipeline de CI, é necessário:

Adicionar um .gitlab-ci.yml arquivo ao diretório raiz do reposirório.

O projeto foi configurado para usar um runner.


### O .gitlab-ci.yml arquivo define a estrutura e a ordem dos pipelines e determina:

- O que executar usando o GitLab Runner .
- Que decisões tomar quando forem encontradas condições específicas. Por exemplo, quando um processo é bem-sucedido ou falha.

- Um pipeline simples geralmente tem três estágios:

* build
* test
* deploy

0 Não precisa usar todos os três estágios; estágios sem jobs são ignorados.

### Etapa 1: Criação de um .gitlab-ci.yml arquivo
- O .gitlab-ci.yml arquivo é onde você configura o que o CI faz com seu projeto. Ele reside na raiz do seu repositório.

- Criando um .gitlab-ci.yml arquivo simples

```
default:
  image: ruby:2.5
  before_script:
    - apt-get update
    - apt-get install -y sqlite3 libsqlite3-dev nodejs
    - ruby -v
    - which ruby
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}"

rspec:
  script:
    - bundle exec rspec

rubocop:
  script:
    - bundle exec rubocop
``` 

Esta é a configuração mais simples possível que funcionará para a maioria dos aplicativos Ruby:

Defina duas tarefas rspece rubocop(os nomes são arbitrários) com diferentes comandos a serem executados.

Antes de cada trabalho, os comandos definidos por before_scriptsão executados.

O .gitlab-ci.ymlarquivo define conjuntos de trabalhos com restrições de como e quando devem ser executados. Os trabalhos são definidos como elementos de nível superior com um nome (em nosso caso rspece rubocop) e sempre devem conter a scriptpalavra - chave.

O importante é que cada trabalho seja executado independentemente um do outro.

### Etapa 2: Empurre .gitlab-ci.yml a gitlab

Depois de criar um .gitlab-ci.yml, deve adicioná-lo ao seu repositório Git e enviá-lo ao GitLab.

```
* $ git add .gitlab-ci.yml
* $ git commit -m "Add .gitlab-ci.yml"
* $ git push origin master
```

A página Pipelines, verá que o pipeline está pendente.

A próxima etapa é configurar um runner para que ele escolha os trabalhos pendentes.

### Etapa 3: Configurar um Runner

No GitLab, os executores executam as tarefas que você define em .gitlab-ci.yml. Um runner pode ser uma máquina virtual, um VPS, uma máquina bare-metal, um contêiner Docker ou mesmo um cluster de contêineres. 
O GitLab e o runner se comunicam por meio de uma API, portanto, o único requisito é que a máquina do runner tenha acesso de rede ao servidor GitLab.

Um runner pode ser específico para um determinado projeto ou servir a vários projetos no GitLab. Se servir a todos os projetos, é chamado de corredor compartilhado .


### Instalando o Runner: 

- Buscar versão mais recente:

* $ curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_<arch>.deb
* $ dpkg -i gitlab-runner_<arch>.deb

- Adicionando permissões
* $ sudo chmod +x /usr/local/bin/gitlab-runner

* $ Configurando o Runner específico manualmente

* $ gitlab-runner register
```
Runtime platform                                    arch=amd64 os=linux pid=58447 revision=ece86343 version=13.5.0
                                                   
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com

Please enter the gitlab-ci token for this runner:
B2hSxoEUkx3ZxGCrmy_h

Please enter the gitlab-ci description for this runner:
[jenniemoura]: Linux

Please enter the gitlab-ci tags for this runner (comma separated):

Registering runner... succeeded                     runner=B2hSxoEU
Please enter the executor: custom, parallels, ssh, virtualbox, kubernetes, docker, docker-ssh, shell, docker+machine, docker-ssh+machine:
docker
Please enter the default Docker image (e.g. ruby:2.6):
ruby:2.6
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```
Quando um runner está disponível, você pode visualizá-lo clicando em Configurações> CI / CD e expandindo Runners .

No painel gitlab ir no campo > Settings > CI/CD > Variables expand:

Criando as variáveis no painel do gitlab-ci mascarado	Ambientes: 
```
Variável
AWS_ACCESS_KEY_ID
*********************
Variável
AWS_SECRET_KEY
*********************
Variável
CI_REGISTRY_PASSWORD
*********************
Variável
CI_REGISTRY_USER
*********************
Variável
KUBE_CONFIG
*********************
Variável
OPAC_CACHE_REDIS_HOST
*********************
Variável
OPAC_CACHE_REDIS_PORT
*********************
```
### Conectando no cluster Kubernetes
```
* $ kubectl create namespace jennie-desafio-06
namespace/jennie-desafio-06 created

*$ kubectl get ns
NAME                  STATUS   AGE
jennie-desafio-06     Active   3s
```







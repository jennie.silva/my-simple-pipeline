from flask import Flask
from redis import Redis
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from rq import Queue
import os

host_redis = os.environ.get('HOST_REDIS', '45.62.234.63')
port_redis = os.environ.get('PORT_REDIS', 6379)

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
# Build the Sqlite ULR for SqlAlchemy
sqlite_url = "sqlite:////" + os.path.join(basedir, "ProjetoAPI.db")

q = Queue(connection=Redis())

# Configure the SqlAlchemy part of the app instance
app.config["SQLALCHEMY_ECHO"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = sqlite_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
redis = Redis(host=host_redis, port=port_redis)

@app.route('/')
@app.route('/rota2')
def status():
#    redis.incr('hits')
    dict_api = {
      'api': 2.0,
      'dep': {
      'db-sql': status_db(),
      'no-sql': no_sql(),
      'fila':  status_fila(),

      },
    }
    message = {
        'API': dict_api,
        'status': 200,

    }
    resp = jsonify(message)
    resp.status_code = 200
    print(resp)
    return resp


def no_sql( ):
    """

    """
    status_redis = redis.ping()
    if status_redis:
        return "OK"
    else:
        return "Não - OK"

#funcao de teste do sql
def status_db():
    try:
        print(db.session.query("1").all())
        return 'OK'
    except:
        return 'Não - OK'


def status_fila():
    Queue = Redis()
    if Queue:
        return ('OK')
    else:
        return ('Não - OK')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
#!/bin/bash

echo "Validando se há um ingress em andamento..."

validando=$(kubectl describe ingress -n jennie-desafio-06 example-ingress| wc -l)

# shellcheck disable=SC1009
# shellcheck disable=SC1073
if [ $validando != "0" ]; then
  echo "Há um ingress em execução (/)"
  versao1=$(kubectl describe ingress -n jennie-desafio-06 example-ingress | grep / | awk {'print $1'} | head -n 1)
  echo -e "Ingress em andamento é a versão Blue, a versão que queremos é Green"
  if [ $versao1 = '/' ]; then
    echo "Realizando deploy para o green"
    envsubst < deployment-green.yaml | kubectl apply -f - --record
    kubectl apply -f services-green.yaml
  else
    echo "Realizando deploy para o blue"
    envsubst < deployment.yaml | kubectl apply -f - --record
    kubectl apply -f service.yaml
  fi
else
  echo -e "Não há nenhum ingress em andamento"
    echo -e "Realizando deploy do ambiente Green"
    envsubst < deployment-green.yaml | kubectl apply -f - --record
    kubectl apply -f ingress-green.yaml
    kubectl apply -f services-green.yaml
    echo
    echo -e "Realizando deploy do ambiente Blue"
    envsubst < deployment.yaml | kubectl apply -f - --record
    kubectl apply -f ingress.yaml
    kubectl apply -f service.yaml
fi

sleep 10
versao=$(curl -s http://a677f8823ece44f13bb8a8fdd2a42e1e-6ac1d5ac829c74f1.elb.us-east-1.amazonaws.com/rota2 2>&1 | grep '\"api\":\ *.*' | cut -d: -f 2| cut -d, -f1)
echo -e "O deploy em execução se encontra na versão $versao"

echo

if [ $versao = "2.0" ]
then
    echo "Realizando alteracao das rotas"
    rota_green=$(kubectl describe ingress -n jennie-desafio-06 example-ingress | grep / | awk {'print $1'} | head -n 1)
    if [ $rota_green = "/" ]
    then
        kubectl apply -f ingress-green.yaml
        echo -e "rota alterada"
    else
        kubectl apply -f ingress.yaml
        echo -e $rota_green
        echo -e "rota alterada"
    fi
else
    echo -e "Fazendo rollback, o teste falhou" $version
    exit 4
fi



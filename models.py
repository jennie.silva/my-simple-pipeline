from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime



engine = create_engine ('sqlite:///api.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

class API(Base):
    __tablename__ = "api"
    id = Column(Integer, primary_key=True)
    name = Column(String(32) , index=True)
    name = Column(String(32) , index=True)

def __repr__(self):
    return '<API {}>'.formart(self.nome)

def init_db():
    Base.metadata.create_all(bind=engine)

if __name__ == '__main__':
    init_db()
